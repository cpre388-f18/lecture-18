package edu.iastate.cpre388.lecture18_intentreceiver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class PhoneActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);

        TextView phoneTV = findViewById(R.id.phoneTextView);
        String uri = getIntent().getData().toString();
        phoneTV.setText(uri);
    }
}
