package edu.iastate.cpre388.lecture18_implictiintents;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CONTACT = 1;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onUrlClick(View v) {
        // An intent with an action (ACTION_VIEW) and data of a URL (https://iastate.edu)
        Intent explicitIntent = new Intent(Intent.ACTION_VIEW);
        explicitIntent.setData(Uri.parse("https://iastate.edu"));
        startActivity(explicitIntent);
    }

    public void onUriClick(View v) {
        // An intent with an action (ACTION_VIEW) and data of a URI (mailto:jmay@iastate.edu)
        Intent explicitIntent = new Intent(Intent.ACTION_VIEW);
        explicitIntent.setData(Uri.parse("mailto:jmay@iastate.edu"));
        startActivity(explicitIntent);
    }

    public void onTextClick(View v) {
        // An intent with an action (ACTION_SEND) and data in the extra bundle.
        // The send action is with a type of "text/plain", but no URI.
        Intent explicitIntent = new Intent(Intent.ACTION_SEND);
        explicitIntent.putExtra(Intent.EXTRA_TEXT, "hello world");
        explicitIntent.setType("text/plain");
        startActivity(explicitIntent);
    }

    public void onTextChooserClick(View v) {
        // Same as onTextClick
        Intent explicitIntent = new Intent(Intent.ACTION_SEND);
        explicitIntent.putExtra(Intent.EXTRA_TEXT, "hello world");
        explicitIntent.setType("text/plain");

        // Force a chooser instead of using the default app.  Note that this chooser takes care of
        // launching the explicitIntent.
        Intent chooserIntent = Intent.createChooser(explicitIntent, "Chooser Title");

        startActivity(chooserIntent);
    }

    public void onUnknownClick(View v) {
        // This is intended to show what happens when the phone does not have an app to receive
        // the intent.
        Intent explicitIntent = new Intent(Intent.ACTION_VIEW);
        explicitIntent.setData(Uri.parse("isbn:978-0345453747"));

        // This type of check should be done on *ALL* implicit intents.
        if (explicitIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(explicitIntent);
        } else {
            // Take an appropriate action with the knowledge that there is no app to receive the
            // intent.
            Toast.makeText(this, "No app installed that handles ISBNs", Toast.LENGTH_LONG).show();
        }
    }

    public void onPhoneClick(View v) {
        // A simple intent with an ACTION_VIEW of a telephone URI.
        Intent explicitIntent = new Intent(Intent.ACTION_VIEW);
        explicitIntent.setData(Uri.parse("tel:1234567890"));
        startActivity(explicitIntent);
    }

    public void onContactClick(View v) {
        // The documentation on this is available here:
        // https://developer.android.com/guide/topics/providers/contacts-provider#Intents
        Intent explicitIntent = new Intent(Intent.ACTION_PICK);
        explicitIntent.setData(ContactsContract.Contacts.CONTENT_URI);
        // Note that the contact picker is started for result, which will raise a callback to the
        // onActivityResult function.
        startActivityForResult(explicitIntent, REQUEST_CONTACT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONTACT:
                // Result from onContactClick

                // Check that the user chose a contact, rather than canceling the operation.
                if (resultCode == Activity.RESULT_OK) {
                    // Get the contact URI from the "data" component from the intent
                    String contactURI = data.getDataString();
                    Toast.makeText(this, contactURI, Toast.LENGTH_LONG).show();
                    Log.i(TAG, contactURI);
                }
        }
    }
}
